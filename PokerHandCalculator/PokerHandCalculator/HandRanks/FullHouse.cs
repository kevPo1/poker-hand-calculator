﻿using System;
using System.Collections.Generic;
using System.Linq;
using PokerHandCalculator.Cards;

namespace PokerHandCalculator.HandRanks
{
    public class FullHouse : IHandRank
    {
        private const Int32 NUMBER_OF_CARDS_NEEDED = 5;
        private readonly IEnumerable<ICard> cards;

        public FullHouse(IEnumerable<ICard> cards)
        {
            this.cards = cards;
        }

        public Boolean Exists()
        {
            if (cards.Count() != NUMBER_OF_CARDS_NEEDED)
                return false;

            var rankGroupings = cards.GroupBy(c => c.Rank);

            return OnlyTwoRanksArePresent(rankGroupings) && OneRankIsUsedThreeTimes(rankGroupings);
        }

        private Boolean OnlyTwoRanksArePresent(IEnumerable<IGrouping<Int32, ICard>> rankGroupings)
        {
            return rankGroupings.Count() == 2;
        }

        private Boolean OneRankIsUsedThreeTimes(IEnumerable<IGrouping<Int32, ICard>> rankGroupings)
        {
            return rankGroupings.Any(g => g.Count() == 3);
        }

        public String GetName()
        {
            return "FULL HOUSE";
        }
    }
}
