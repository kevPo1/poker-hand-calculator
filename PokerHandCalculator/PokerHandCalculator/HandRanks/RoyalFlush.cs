﻿using System;
using System.Collections.Generic;
using System.Linq;
using PokerHandCalculator.Cards;

namespace PokerHandCalculator.HandRanks
{
    public class RoyalFlush : IHandRank
    {
        private const Int32 HIGH_CARD = 14;
        private readonly IEnumerable<ICard> cards;

        public RoyalFlush(IEnumerable<ICard> cards)
        {
            this.cards = cards;
        }

        public Boolean Exists()
        {
            if (cards.Any() == false)
                return false;

            var straightFlush = new StraightFlush(cards);
            var highestRankingCard = cards.Select(c => c.Rank).Max();

            return straightFlush.Exists() && highestRankingCard == HIGH_CARD;
        }

        public String GetName()
        {
            return "ROYAL FLUSH";
        }
    }
}
