﻿using System;
using System.Collections.Generic;
using System.Linq;
using PokerHandCalculator.Cards;

namespace PokerHandCalculator.HandRanks
{
    public class Pair : IHandRank
    {
        private const Int32 NUMBER_OF_CARDS_NEEDED = 2;
        private readonly IEnumerable<ICard> cards;

        public Pair(IEnumerable<ICard> cards)
        {
            this.cards = cards;
        }

        public Boolean Exists()
        {
            if (cards.Count() < NUMBER_OF_CARDS_NEEDED)
                return false;

            return cards.Select(c => c.Rank).Distinct().Count() < cards.Count();
        }

        public String GetName()
        {
            return "PAIR";
        }
    }
}
