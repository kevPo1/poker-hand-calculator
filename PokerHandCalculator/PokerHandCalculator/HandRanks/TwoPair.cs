﻿using System;
using System.Collections.Generic;
using System.Linq;
using PokerHandCalculator.Cards;

namespace PokerHandCalculator.HandRanks
{
    public class TwoPair : IHandRank
    {
        private const Int32 NUMBER_OF_CARDS_NEEDED = 4;
        private readonly IEnumerable<ICard> cards;

        public TwoPair(IEnumerable<ICard> cards)
        {
            this.cards = cards;
        }

        public Boolean Exists()
        {
            if (cards.Count() < NUMBER_OF_CARDS_NEEDED)
                return false;

            return cards.GroupBy(c => c.Rank).Count(g => g.Count() > 1) == 2;
        }

        public String GetName()
        {
            return "TWO PAIR";
        }
    }
}
