﻿using System;
using System.Collections.Generic;
using System.Linq;
using PokerHandCalculator.Cards;

namespace PokerHandCalculator.HandRanks
{
    public class FourOfAKind : IHandRank
    {
        private readonly IEnumerable<ICard> cards;

        public FourOfAKind(IEnumerable<ICard> cards)
        {
            this.cards = cards;
        }

        public Boolean Exists()
        {
            if (cards.Count() < 4)
                return false;

            return cards.GroupBy(c => c.Rank).Any(g => g.Count() > 3);
        }

        public String GetName()
        {
            return "FOUR OF A KIND";
        }
    }
}
