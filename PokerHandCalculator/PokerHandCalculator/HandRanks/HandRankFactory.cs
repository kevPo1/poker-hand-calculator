﻿using System.Collections.Generic;
using PokerHandCalculator.Cards;

namespace PokerHandCalculator.HandRanks
{
    public class HandRankFactory
    {
        public static ICollection<IHandRank> BuildHandRanksForPoker(IEnumerable<ICard> cards)
        {
            return new IHandRank[]
            {
                new RoyalFlush(cards),
                new StraightFlush(cards),
                new FourOfAKind(cards),
                new FullHouse(cards),
                new Flush(cards),
                new Straight(cards),
                new ThreeOfAKind(cards),
                new TwoPair(cards),
                new Pair(cards)
            };
        }

        public static ICollection<IHandRank> BuildHandRanksForThreeCardPoker(IEnumerable<ICard> cards)
        {
            return new IHandRank[]
            {
                new RoyalFlush(cards),
                new StraightFlush(cards),
                new ThreeOfAKind(cards),
                new Straight(cards),
                new Flush(cards),
                new Pair(cards)
            };
        }
    }
}
