﻿using System.Collections.Generic;
using PokerHandCalculator.Cards;
using PokerHandCalculator.HandRanks;

namespace PokerHandCalculator.Hands
{
    public class PokerHandFactory : IHandFactory
    {
        public IHand CreatePokerHand(IEnumerable<ICard> cards)
        {
            var handRanks = HandRankFactory.BuildHandRanksForPoker(cards);

            return new PokerHand(5, cards, handRanks);
        }

        public IHand CreateThreeCardPokerHand(IEnumerable<ICard> cards)
        {
            var handRanks = HandRankFactory.BuildHandRanksForThreeCardPoker(cards);

            return new PokerHand(3, cards, handRanks);
        }
    }
}
