﻿using System;
using System.Collections.Generic;
using System.Linq;
using PokerHandCalculator.Cards;
using PokerHandCalculator.HandRanks;

namespace PokerHandCalculator.Hands
{
    public class PokerHand : IHand
    {
        public Int32 NumberOfCards { get; private set; }

        private readonly IEnumerable<ICard> cards;
        private readonly ICollection<IHandRank> handRanks;

        public PokerHand(Int32 numberOfCards, IEnumerable<ICard> cards, ICollection<IHandRank> handRanks)
        {
            this.NumberOfCards = numberOfCards;
            this.cards = cards;
            this.handRanks = handRanks;
        }

        public String Calculate()
        {
            ValidateHand();

            var rankForHand = handRanks.FirstOrDefault(r => r.Exists());

            if (rankForHand != null)
                return rankForHand.GetName();

            return GetHighCard();
        }

        private void ValidateHand()
        {
            if (cards.Count() != NumberOfCards)
                throw new Exception(String.Format("Must have {0} cards for a valid hand.", NumberOfCards));

            if (cards.Select(c => c.Code).Distinct().Count() != NumberOfCards)
                throw new Exception("Duplicate cards are not valid for poker hand.");
        }

        private String GetHighCard()
        {
            var highCard = cards.Aggregate((current, next) => current.Rank > next.Rank ? current : next);

            return String.Format("{0} HIGH", highCard.Value); 
        }
    }
}
