﻿using System;

namespace PokerHandCalculator.Cards
{
    public interface ICard
    {
        String Code { get; }
        Int32 Rank { get; }
		String Suit { get; }
        String Value { get; }
    }
}
