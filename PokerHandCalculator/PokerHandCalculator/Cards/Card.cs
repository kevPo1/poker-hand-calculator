﻿using System;

namespace PokerHandCalculator.Cards
{
    public class Card : ICard
    {
        public String Code { get; private set; }
        public Int32 Rank { get; private set; }
        public String Suit { get; private set; }
		public String Value { get; private set; }

        public Card(String code, Int32 rank, String suit, String value)
        {
            Code = code;
            Rank = rank;
			Suit = suit;
            Value = value;
        }
    }
}
