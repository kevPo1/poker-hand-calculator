﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PokerHandCalculator.Cards;
using PokerHandCalculator.Hands;

namespace PokerHandCalculatorTests.Integration.Hands
{
    [TestClass]
    public class CalculatorTests
    {
        private ICardRepository cardRepository;
        private IHandFactory handFactory;
        private Calculator calculator;

        public CalculatorTests()
        {
            this.cardRepository = new CardRepository();
            this.handFactory = new PokerHandFactory();
            this.calculator = new Calculator(cardRepository, handFactory);
        }

        [TestMethod]
        public void CalculateReturnsJackHighForJackHighCards()
        {
            Assert.AreEqual("JACK HIGH", calculator.CalculatePokerHand("JS 10D 4H 5C 8H"));
        }

        [TestMethod]
        public void CalculateReturnsPairForCardsWithAPair()
        {
            Assert.AreEqual("PAIR", calculator.CalculatePokerHand("JS JD 4H 5C 10H"));
        }

        [TestMethod]
        public void CalculateReturnsTwoPairForCardsWithTwoPair()
        {
            Assert.AreEqual("TWO PAIR", calculator.CalculatePokerHand("JS 4D 4H 10C 10H"));
        }

        [TestMethod]
        public void CalculateReturnsThreeOfAKindForCardsWithThreeOfAKind()
        {
            Assert.AreEqual("THREE OF A KIND", calculator.CalculatePokerHand("JS JC JH 5C 10H"));
        }

        [TestMethod]
        public void CalculateReturnsStraightForCardsWithSequentialRanks()
        {
            Assert.AreEqual("STRAIGHT", calculator.CalculatePokerHand("7S 8S 9S 10C JH"));
        }

        [TestMethod]
        public void CalculateReturnsFlushForCardsWithSameSuit()
        {
            Assert.AreEqual("FLUSH", calculator.CalculatePokerHand("7S 4S 9S 10S JS"));
        }

        [TestMethod]
        public void CalculateReturnsFullHouseForCardsThreeWithOneRankAndTwoWithAnother()
        {
            Assert.AreEqual("FULL HOUSE", calculator.CalculatePokerHand("7S 7D 7C JC JS"));
        }

        [TestMethod]
        public void CalculateReturnsFourOfAKindWhenFourCardsHaveSameRank()
        {
            Assert.AreEqual("FOUR OF A KIND", calculator.CalculatePokerHand("7S 7D 7C 7H JS"));
        }

        [TestMethod]
        public void CalculateReturnsStraightFlushWhenCardsHaveSameSuitAndAreSequenced()
        {
            Assert.AreEqual("STRAIGHT FLUSH", calculator.CalculatePokerHand("7S 8S 9S 10S JS"));
        }

        [TestMethod]
        public void CalculateReturnsRoyalFlushWhenCardsHaveSameSuitAndAreSequencedAceHigh()
        {
            Assert.AreEqual("ROYAL FLUSH", calculator.CalculatePokerHand("AS KS QS 10S JS"));
        }
    }
}
