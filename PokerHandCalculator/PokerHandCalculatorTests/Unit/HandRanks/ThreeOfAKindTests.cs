﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PokerHandCalculator.Cards;
using PokerHandCalculator.HandRanks;

namespace PokerHandCalculatorTests.Unit.HandRanks
{
    [TestClass]
    public class ThreeOfAKindTests
    {
        [TestMethod]
        public void GetNameReturnsThreeOfAKind()
        {
            var threeOfAKindRank = new ThreeOfAKind(Enumerable.Empty<ICard>());

            Assert.AreEqual("THREE OF A KIND", threeOfAKindRank.GetName());
        }

        [TestMethod]
        public void ExistsWithLessThanThreeCardsReturnsFalse()
        {
            var cards = new[]
            {
                new Card("10H", 10, "HEARTS", "TEN"),
                new Card("9H", 9, "HEARTS", "NINE")
            };

            var threeOfAKind = new ThreeOfAKind(cards);

            Assert.IsFalse(threeOfAKind.Exists());
        }

        [TestMethod]
        public void ExistsWithoutThreeOfAKindReturnsFalse()
        {
            var cards = new[]
            {
                new Card("10H", 10, "HEARTS", "TEN"),
                new Card("9H", 9, "HEARTS", "NINE"),
                new Card("9D", 9, "DIAMONDS", "NINE")
            };

            var threeOfAKind = new ThreeOfAKind(cards);

            Assert.IsFalse(threeOfAKind.Exists());
        }

        [TestMethod]
        public void ExistsWithThreeOfAKindReturnsTrue()
        {
            var cards = new[]
            {
                new Card("9S", 9, "SPADES", "NINE"),
                new Card("9H", 9, "HEARTS", "NINE"),
                new Card("9D", 9, "DIAMONDS", "NINE")
            };

            var threeOfAKind = new ThreeOfAKind(cards);

            Assert.IsTrue(threeOfAKind.Exists());
        }
    }
}
