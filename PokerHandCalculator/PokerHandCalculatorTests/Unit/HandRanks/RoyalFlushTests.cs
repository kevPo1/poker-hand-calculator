﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PokerHandCalculator.Cards;
using PokerHandCalculator.HandRanks;

namespace PokerHandCalculatorTests.Unit.HandRanks
{
    [TestClass]
    public class RoyalFlushTests
    {
        [TestMethod]
        public void GetNameReturnsRoyalFlush()
        {
            var royalFlush = new RoyalFlush(Enumerable.Empty<ICard>());

            Assert.AreEqual("ROYAL FLUSH", royalFlush.GetName());
        }

        [TestMethod]
        public void ExistsReturnFalseWhenGivenStraightFlush()
        {
            var cards = new[]
            {
                new Card("4H", 4, "HEARTS", "FOUR"),
                new Card("5H", 5, "HEARTS", "FIVE"),
                new Card("6H", 6, "HEARTS", "SIX"),
                new Card("7H", 7, "HEARTS", "SEVEN"),
                new Card("8H", 8, "HEARTS", "EIGHT")
            };

            var royalFlush = new RoyalFlush(cards);

            Assert.IsFalse(royalFlush.Exists());
        }

        [TestMethod]
        public void ExistsReturnTrueWhenGivenRoyalFlush()
        {
            var cards = new[]
            {
                new Card("AH", 14, "HEARTS", "ACE"),
                new Card("KH", 13, "HEARTS", "KING"),
                new Card("JH", 11, "HEARTS", "JACK"),
                new Card("QH", 12, "HEARTS", "QUEEN"),
                new Card("10H", 10, "HEARTS", "TEN")
            };

            var royalFlush = new RoyalFlush(cards);

            Assert.IsTrue(royalFlush.Exists());
        }
    }
}
