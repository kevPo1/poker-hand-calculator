﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PokerHandCalculator.Cards;
using PokerHandCalculator.HandRanks;

namespace PokerHandCalculatorTests.Unit.HandRanks
{
    [TestClass]
    public class FourOfAKindTests
    {
        [TestMethod]
        public void GetNameReturnsFourOfAKind()
        {
            var fourOfAKindRank = new FourOfAKind(Enumerable.Empty<ICard>());

            Assert.AreEqual("FOUR OF A KIND", fourOfAKindRank.GetName());
        }

        [TestMethod]
        public void ExistsReturnsFalseWhenNotPassedFourCards()
        {
            var fourOfAKindRankers = new[]
            {
                new FourOfAKind(Enumerable.Empty<ICard>()),
                new FourOfAKind(new[] 
                {
                    new Card("KH", 13, "HEARTS", "KING"),
                    new Card("2H", 2, "HEARTS", "TWO"),
                    new Card("5C", 5, "CLUBS", "FIVE")
                })
            };

            foreach (var fourOfAKind in fourOfAKindRankers)
                Assert.IsFalse(fourOfAKind.Exists());
        }

        [TestMethod]
        public void ExistsReturnsFalseWhenCardsDoNotHaveFourOfSameRank()
        {
            var cards = new[]
            {
                new Card("KH", 13, "HEARTS", "KING"),
                new Card("2H", 2, "HEARTS", "TWO"),
                new Card("KD", 13, "DIAMONDS", "KING"),
                new Card("KC", 13, "CLUBS", "KING")
            };

            var fourOfAKind = new FourOfAKind(cards);

            Assert.IsFalse(fourOfAKind.Exists());
        }

        [TestMethod]
        public void ExistsReturnsTrueWhenCardsHaveFourOfSameRank()
        {
            var cards = new[]
            {
                new Card("KH", 13, "HEARTS", "KING"),
                new Card("KS", 13, "SPADES", "KING"),
                new Card("KD", 13, "DIAMONDS", "KING"),
                new Card("KC", 13, "CLUBS", "KING")
            };

            var fourOfAKind = new FourOfAKind(cards);

            Assert.IsTrue(fourOfAKind.Exists());
        }
    }
}
