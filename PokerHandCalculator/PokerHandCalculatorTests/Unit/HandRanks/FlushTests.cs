﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PokerHandCalculator.Cards;
using PokerHandCalculator.HandRanks;

namespace PokerHandCalculatorTests.Unit.HandRanks
{
    [TestClass]
    public class FlushTests
    {
        [TestMethod]
        public void GetNameReturnsFlush()
        {
            var flush = new Flush(Enumerable.Empty<ICard>());

            Assert.AreEqual("FLUSH", flush.GetName());
        }

        [TestMethod]
        public void ExistsReturnsFalseWhenNoCardsArePassed()
        {
            var flush = new Flush(Enumerable.Empty<ICard>());

            Assert.IsFalse(flush.Exists());
        }

        [TestMethod]
        public void ExistsReturnsFalseWhenAllCardsDoNotHaveSameSuit()
        {
            var cards = new[]
            {
                new Card("10H", 10, "HEARTS", "TEN"),
                new Card("9H", 9, "HEARTS", "NINE"),
                new Card("8D", 8, "DIAMONDS", "EIGHT")
            };

            var flush = new Flush(cards);

            Assert.IsFalse(flush.Exists());
        }

        [TestMethod]
        public void ExistsReturnsTrueWhenAllCardsHaveSameSuit()
        {
            var cards = new[]
            {
                new Card("10H", 10, "HEARTS", "TEN"),
                new Card("9H", 9, "HEARTS", "NINE"),
                new Card("6H", 6, "HEARTS", "SIX")
            };

            var flush = new Flush(cards);

            Assert.IsTrue(flush.Exists());
        }
    }
}
