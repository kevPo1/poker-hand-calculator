﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PokerHandCalculator.Cards;
using PokerHandCalculator.HandRanks;

namespace PokerHandCalculatorTests.Unit.HandRanks
{
    [TestClass]
    public class TwoPairTests
    {
        [TestMethod]
        public void GetNameReturnsTwoPair()
        {
            var twoPair = new TwoPair(Enumerable.Empty<ICard>());

            Assert.AreEqual("TWO PAIR", twoPair.GetName());
        }

        [TestMethod]
        public void ExistsReturnsFalseWhenNoPairsExist()
        {
            var twoPair = new TwoPair(Enumerable.Empty<ICard>());

            Assert.IsFalse(twoPair.Exists());
        }

        [TestMethod]
        public void ExistsReturnsFalseWhenOnePairExists()
        {
            var cards = new[]
            {
                new Card("10H", 10, "HEARTS", "TEN"),
                new Card("10D", 10, "DIAMONDS", "TEN"),
                new Card("KH", 13, "HEARTS", "KING"),
                new Card("2H", 2, "HEARTS", "TWO")
            };

            var twoPair = new TwoPair(cards);

            Assert.IsFalse(twoPair.Exists());
        }

        [TestMethod]
        public void ExistsReturnsTrueWhenTwoPairsExist()
        {
            var cards = new[]
            {
                new Card("10H", 10, "HEARTS", "TEN"),
                new Card("10D", 10, "DIAMONDS", "TEN"),
                new Card("KH", 13, "HEARTS", "KING"),
                new Card("KD", 13, "DIAMONDS", "KING")
            };

            var twoPair = new TwoPair(cards);

            Assert.IsTrue(twoPair.Exists());
        }
    }
}
