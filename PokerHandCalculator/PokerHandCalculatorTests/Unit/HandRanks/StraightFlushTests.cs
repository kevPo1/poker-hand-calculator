﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PokerHandCalculator.Cards;
using PokerHandCalculator.HandRanks;

namespace PokerHandCalculatorTests.Unit.HandRanks
{
    [TestClass]
    public class StraightFlushTests
    {
        [TestMethod]
        public void GetNameReturnsStraightFlush()
        {
            var straightFlush = new StraightFlush(Enumerable.Empty<ICard>());

            Assert.AreEqual("STRAIGHT FLUSH", straightFlush.GetName());
        }

        [TestMethod]
        public void ExistsReturnsFalseWhenPassedNoCards()
        {
            var straightFlush = new StraightFlush(Enumerable.Empty<ICard>());

            Assert.IsFalse(straightFlush.Exists());
        }

        [TestMethod]
        public void ExistsReturnsFalseWhenPassedStraight()
        {
            var cards = new[]
            {
                new Card("4H", 4, "HEARTS", "FOUR"),
                new Card("5H", 5, "HEARTS", "FIVE"),
                new Card("6D", 6, "DIAMONDS", "SIX"),
                new Card("7H", 7, "HEARTS", "SEVEN"),
                new Card("8H", 8, "HEARTS", "EIGHT")
            };

            var straightFlush = new StraightFlush(cards);

            Assert.IsFalse(straightFlush.Exists());
        }

        [TestMethod]
        public void ExistsReturnsFalseWhenPassedFlush()
        {
            var cards = new[]
            {
                new Card("4H", 4, "HEARTS", "FOUR"),
                new Card("5H", 5, "HEARTS", "FIVE"),
                new Card("2H", 2, "HEARTS", "TWO"),
                new Card("7H", 7, "HEARTS", "SEVEN"),
                new Card("KH", 13, "HEARTS", "KING")
            };

            var straightFlush = new StraightFlush(cards);

            Assert.IsFalse(straightFlush.Exists());
        }

        [TestMethod]
        public void ExistsReturnsTrueWhenPassedStraightFlush()
        {
            var cards = new[]
            {
                new Card("4H", 4, "HEARTS", "FOUR"),
                new Card("5H", 5, "HEARTS", "FIVE"),
                new Card("6H", 6, "HEARTS", "SIX"),
                new Card("7H", 7, "HEARTS", "SEVEN"),
                new Card("8H", 8, "HEARTS", "EIGHT")
            };

            var straightFlush = new StraightFlush(cards);

            Assert.IsTrue(straightFlush.Exists());
        }
    }
}
