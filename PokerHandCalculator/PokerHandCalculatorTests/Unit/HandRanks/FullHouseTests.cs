﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PokerHandCalculator.Cards;
using PokerHandCalculator.HandRanks;

namespace PokerHandCalculatorTests.Unit.HandRanks
{
    [TestClass]
    public class FullHouseTests
    {
        [TestMethod]
        public void GetNameReturnsFullHouse()
        {
            var fullHouse = new FullHouse(Enumerable.Empty<ICard>());

            Assert.AreEqual("FULL HOUSE", fullHouse.GetName());
        }

        [TestMethod]
        public void ExistsReturnsFalseWhenNotGivenFiveCards()
        {
            var fullHouseRankers = new[]
            {
                new FullHouse(Enumerable.Empty<ICard>()),
                new FullHouse(new[] 
                {
                    new Card("KH", 13, "HEARTS", "KING"),
                    new Card("KS", 13, "SPADES", "KING"),
                    new Card("KD", 13, "DIAMONDS", "KING"),
                    new Card("KC", 13, "CLUBS", "KING")
                }),
                new FullHouse(new[] 
                {
                    new Card("KH", 13, "HEARTS", "KING"),
                    new Card("KS", 13, "SPADES", "KING"),
                    new Card("KD", 13, "DIAMONDS", "KING"),
                    new Card("KC", 13, "CLUBS", "KING"),
                    new Card("2C", 2, "CLUBS", "TWO"),
                    new Card("2H", 2, "HEARTS", "TWO")
                })
            };

            foreach (var fullHouse in fullHouseRankers)
                Assert.IsFalse(fullHouse.Exists());
        }

        [TestMethod]
        public void ExistsReturnsFalseWhenFullHouseIsNotPresent()
        {
            var cards = new[]
            {
                new Card("KH", 13, "HEARTS", "KING"),
                new Card("KS", 13, "SPADES", "KING"),
                new Card("KD", 13, "DIAMONDS", "KING"),
                new Card("KC", 13, "CLUBS", "KING"),
                new Card("2C", 2, "CLUBS", "TWO")
            };

            var fullHouse = new FullHouse(cards);

            Assert.IsFalse(fullHouse.Exists());
        }

        [TestMethod]
        public void ExistsReturnsTrueWhenFullHouseIsPresent()
        {
            var cards = new[]
            {
                new Card("KH", 13, "HEARTS", "KING"),
                new Card("KS", 13, "SPADES", "KING"),
                new Card("KD", 13, "DIAMONDS", "KING"),
                new Card("2C", 2, "CLUBS", "TWO"),
                new Card("2H", 2, "HEARTS", "TWO")
            };

            var fullHouse = new FullHouse(cards);

            Assert.IsTrue(fullHouse.Exists());
        }
    }
}
