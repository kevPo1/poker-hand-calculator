﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PokerHandCalculator.Cards;
using PokerHandCalculator.HandRanks;

namespace PokerHandCalculatorTests.Unit.HandRanks
{
    [TestClass]
    public class StraightTests
    {
        [TestMethod]
        public void GetNameReturnsStraight()
        {
            var straight = new Straight(Enumerable.Empty<ICard>());

            Assert.AreEqual("STRAIGHT", straight.GetName());
        }

        [TestMethod]
        public void ExistsReturnsFalseWhenPassedNoCards()
        {
            var straight = new Straight(Enumerable.Empty<ICard>());

            Assert.IsFalse(straight.Exists());
        }

        [TestMethod]
        public void ExistsReturnsFalseWhenDuplicateRanksExist()
        {
            var cards = new[]
            {
                new Card("10H", 10, "HEARTS", "TEN"),
                new Card("9H", 9, "HEARTS", "NINE"),
                new Card("8D", 8, "DIAMONDS", "EIGHT"),
                new Card("8S", 8, "SPADES", "EIGHT")
            };

            var straight = new Straight(cards);

            Assert.IsFalse(straight.Exists());
        }

        [TestMethod]
        public void ExistsReturnsTrueWhenAllRanksAreSequential()
        {
            var cards = new[]
            {
                new Card("10H", 10, "HEARTS", "TEN"),
                new Card("9H", 9, "HEARTS", "NINE"),
                new Card("8D", 8, "DIAMONDS", "EIGHT"),
                new Card("7S", 7, "SPADES", "SEVEN")
            };

            var straight = new Straight(cards);

            Assert.IsTrue(straight.Exists());
        }

        [TestMethod]
        public void ExistsReturnsTrueForAceLowStraight()
        {
            var cards = new[]
            {
                new Card("3H", 3, "HEARTS", "THREE"),
                new Card("2S", 2, "SPADES", "TWO"),
                new Card("4D", 4, "DIAMONDS", "FOUR"),
                new Card("AS", 14, "SPADES", "ACE")
            };

            var straight = new Straight(cards);

            Assert.IsTrue(straight.Exists());
        }
    }
}
