﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PokerHandCalculator.Cards;
using PokerHandCalculator.Hands;

namespace PokerHandCalculatorTests.Unit.Hands
{
    [TestClass]
    public class PokerHandFactoryTests
    {
        private IEnumerable<ICard> cards;
        private IHandFactory pokerHandFactory;

        public PokerHandFactoryTests()
        {
            pokerHandFactory = new PokerHandFactory();
        }

        [TestInitialize]
        public void Initialize()
        {
            cards = new[]
            {
                new Card("AH", 14, "SPADES", "ACE"),
                new Card("KH", 13, "SPADES", "KING"),
                new Card("QH", 12, "SPADES", "QUEEN"),
                new Card("JH", 11, "SPADES", "JACK"),
                new Card("10H", 10, "SPADES", "TEN")
            };
        }

        [TestMethod]
        public void CreatePokerHandCreatesHandWithFiveCards()
        {
            var pokerHand = pokerHandFactory.CreatePokerHand(cards);

            Assert.AreEqual(5, pokerHand.NumberOfCards);
        }

        [TestMethod]
        public void CreateThreeCardPokerHandCreatesHandWithThreeCards()
        {
            var threeCards = cards.Take(3);
            var threeCardPokerHand = pokerHandFactory.CreateThreeCardPokerHand(threeCards);

            Assert.AreEqual(3, threeCardPokerHand.NumberOfCards);
        }
    }
}
